﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Singletons/MasterManager")]
public class MasterManager : SingletonScriptableObject<MasterManager>
{
    [SerializeField]
    private Gamesetting _gameSetting;

    public static Gamesetting GameSetting { get { return Instance._gameSetting; } }
}
